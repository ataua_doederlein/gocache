# GoCache tech test

## About the project

This project was build as a technical test for a front end developer job appliance at [GoCache](https://www.gocache.com.br/), as of Feb, 2021.

The main goal here is to build a visual platform which would allow users to manage their production environment. In order for that to work, raw data was fetched from the server API by means of a HTTP request, treated accordingly to its contents in a dynamic way, so making it possible to alter the number and characteristics of different fields in the future.
The user can then generate a JSON file with their options updated.

## About the author

My name is Atauã Pinali Doederlein, I live in Curitiba and I aim to work with something I love - which is web development!

## Running the project

1. See a live preview online:

    - head to [gocache.vercel.app](http://gocache.vercel.app)

2. Cloning in your own machine:

    Open a terminal (on Linux) and type:

    ```bash
    git clone git@gitlab.com:ataua_doederlein/gocache.git
    ```

    enter the newly added folder:

    ```bash
    cd gocache
    ```

    install the dependencies (choose one alternative):

    `You should already have` _Node_ ` installed in your machine. In case you still don't have it, follow instructions on` https://nodejs.org/en/download/package-manager/

    - with Node:

    ```bash
    npm install
    ```

    If you prefer using Yarn, because it is better and smarter, get it on https://classic.yarnpkg.com/en/docs/install. (You also need Node in this case)

    - with Yarn:

    ```bash
    yarn install

    # or, simply:
    yarn
    ```

    After installation is complete, run the following command to run the application:

    - with Node:

    ```bash
    npm start
    ```

    - with Yarn:

    ```bash
    yarn start
    ```

    Either option will run your application on port 3000. Open your browser on the address `localhost:3000`.

## Decisions made about the project

1. CORS on the GoCache server did not allow fetching from localhost (development environment), so I could not test the application as I wanted during development. Thus, I opted for dealing with the JSON file.

2. To simulate an api call, state is populated from file after 1.5 seconds. Once populated, data is iterated in a map and passed to a `while` statement, which returns the correct type of input to the render, for each `inputType` found.

3. Some types of inputs, like date / datetime, are not supported by some browsers. I have searched for a way to test this situation and found a function that helped me. I did not test it though, since I have no gadgets with those browsers. But the code aims to identify those situations and then deliver a text input with the correct validation.

4. The rendered form on the real DOM keeps track of its changes and the onchange event has its target on the input where the change happens. This behaviour was used to register the values of changes and then generate a state that would provide data for the file generation (or a form submit if it was the cace). Would the form not keep track of its changes, this behaviour would have to be passed to Redux,as the only way to get data from a nested component.

## Things that could be done but weren't

-   **Componentization of inputs**: I thought about making a component (or an 'atom') for each input type. That would have made the logic of keeping track of all modifications a little harder, and maybe giving support to the code would also become harder, because part of the logic and the styles are shared by different input types.

-   **Validation with Yup**: I thought about using Yup for validating form data, but each generated input would have to provide its own data regarding it being required, regexp pattern or default data. As it was not the case, validation seemed unnecessary for all inputs, so I made a basic validation only in the cases where the provided data was enough to make it possible.

-   **State managing on Redux**: I actually started the project by sending data dto Redux, but it would have a very basic structure, it would even not need to have actions(!), so I have decided to meke thing simpler. Mad some tests with the ability of the tag form to keep track of its content and changes and, since it worked, it seemed easier and cleaner go go that way. But, if things escalate and become confuse, using Redux is a good option. For example, validation could be implemented in the Thunk staging area, which also could be used to fetch data to the server.
