import styled from 'styled-components'

const Button = ({ onClick, children }) => {
	return <StyledButton onClick={onClick}>{children}</StyledButton>
}

const StyledButton = styled.button`
	font-family: 'abz';
	font-size: 14;
	font-weight: 800;
	padding: 10px 14px;
	color: white;
	background-color: #4a749f;
	border-radius: 6px;
	margin-top: 20px;
	align-self: center;
	:hover {
		background-color: #f5633d;
	}
`
export default Button
