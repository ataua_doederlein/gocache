import { Input } from '..'
import './category.css'

const Category = ({ data }) => {
	return (
		<>
			<h2>{data.label}</h2>
			{data.items.map((item, key) => (
				<Input key={key} inputData={item} />
			))}
		</>
	)
}

export default Category
