import './input.css'
import { useState } from 'react'
import { MdError } from 'react-icons/md'
import Tooltip from '@material-ui/core/Button'

const isDateSupported = (type) => {
	// function to test wether the browser supports the <input type=time> tag
	// (found this on https://gomakethings.com/how-to-check-if-a-browser-supports-native-input-date-pickers/)
	const dateInput = document.createElement('input')
	const value = 'whatever'
	dateInput.setAttribute('type', type)
	dateInput.setAttribute('value', value)
	return dateInput.value !== value
}

const Input = ({ inputData }) => {
	const [range, setRange] = useState(
		// set value for input range
		inputData.default ? inputData.default : 1
	)
	const [checked, setChecked] = useState(true) // exclusive for use with input type 'checkbox'
	const [errorMsg, setErrorMsg] = useState(false)

	const validate = ({ target }) => {
		// text regex pattern tast
		const regex = new RegExp(`${target.pattern}`)
		if (!regex.test(target.value)) {
			setErrorMsg(true)
		} else {
			setErrorMsg(false)
		}
	}

	const makeInput = () => {
		switch (inputData.itemType) {
			case 'date':
				return isDateSupported('date') ? (
					<input id={inputData.id} name={inputData.id} type="date" />
				) : (
					// In case if browser does not support input type=date, render
					// a normal text input with a placeholder
					<>
						<input
							id={inputData.id}
							name={inputData.id}
							type="text"
							placeholder={inputData.format}
							pattern="^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$"
							onChange={validate}
						/>
						{errorMsg && (
							<Tooltip title='Utilize o formato "DD/MM/AAAA"'>
								<MdError />
							</Tooltip>
						)}
					</>
				)

			case 'dateTime':
				return isDateSupported('datetime-local') ? (
					<input
						id={inputData.id}
						name={inputData.id}
						type="datetime-local"
					/>
				) : (
					// same as before
					<>
						<input
							id={inputData.id}
							name={inputData.id}
							type="text"
							placeholder={inputData.format}
							pattern="^[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-2][0-9]:[0-5][0-9]$"
							onChange={validate}
						/>
						{errorMsg && (
							<Tooltip title='Utilize o formato "DD/MM/AAAA HH:MM"'>
								<MdError />
							</Tooltip>
						)}
					</>
				)

			case 'numberRange':
				return (
					<>
						<input
							id={inputData.id}
							name={inputData.id}
							type="range"
							max={inputData.maximum}
							min={inputData.minimum}
							defaultValue={inputData.default}
							onChange={(e) => setRange(e.target.value)} // sets the value and shows updated in the span
						/>
						<span className="range">{range}</span>
					</>
				)

			case 'singleOption':
				return (
					<div>
						{inputData.validOptions.map((option, key) => (
							<label key={key} className="radio">
								<input
									type="radio"
									id={option.label}
									name={inputData.id.replace(' ', '_')} // maybe a typo in the original file
									value={option.id}
									defaultChecked={option.default}
								/>
								<span>{option.label}</span>
							</label>
						))}
					</div>
				)

			case 'multipleOption':
				return (
					<select
						id={inputData.id}
						name={inputData.id}
						multiple={true}
					>
						{inputData.validOptions.map((option, key) => (
							<option
								key={`opt-${key}`}
								id={option.id}
								name={option.id}
								value={option.id}
							>
								{option.label}
							</option>
						))}
					</select>
				)

			case 'checkbox':
				return (
					<input
						type="checkbox"
						id={inputData.id}
						name={inputData.id}
						value={checked} // without this workaround, it was behaving weird, always returning value = 'on'
						onChange={() => setChecked(!checked)}
					/>
				)

			default:
				return (
					<>
						<input
							id={inputData.id}
							name={inputData.id}
							type="text"
							pattern={inputData.regex_validation}
							onChange={validate}
							placeholder={
								inputData.id === 'url'
									? 'http://www.abc.def.gh'
									: ''
							}
						/>
						{errorMsg && (
							<Tooltip title="Formato incorreto.">
								<MdError />
							</Tooltip>
						)}
					</>
				)
		}
	}

	return (
		<>
			<label className="inputName">
				<h4>{inputData.label}</h4>
				{makeInput()}
			</label>
		</>
	)
}

export default Input
