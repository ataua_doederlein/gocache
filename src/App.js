import logo from './assets/img/logo.webp'
import Form from './pages/form'
import GlobalStyle from './global-style'

const App = () => {
	return (
		<div className="App">
			<GlobalStyle />
			<header>
				<img src={logo} className="App-logo" alt="logo" />
			</header>
			<main>
				<Form />
			</main>
		</div>
	)
}

export default App
