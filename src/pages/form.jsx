import { useState, useEffect } from 'react'
import { Category, Button } from '../components'
import file from '../assets/data.json'
import './form.css'
// import axios from 'axios'

const Form = () => {
	const [data, setData] = useState([])
	const [formData, setFormData] = useState()

	// fetching the API    ---->    did not work due to CORS config
	// useEffect(() => {
	// 	axios
	// 		.get('http://www.gocache.com.br/public/fe-catitems.json')
	// 		.then((res) => setFormData(res.data))
	// }, [])

	const saveFile = (e) => {
		// function to generate the JSON file
		e.preventDefault()
		const a = document.createElement('a')
		a.href = URL.createObjectURL(
			new Blob([JSON.stringify(formData, null, 2)], {
				type: 'text/plain'
			})
		)
		a.setAttribute('download', 'gocache-config.json')
		a.click()
	}

	useEffect(() => {
		// populates state 'data' with file on app start

		setTimeout(() => {
			setData(file)
		}, 1200)
	})

	useEffect(() => {
		// after 'data' receives file contents, sets initial values (all null) for form

		let baseData = {}
		data.forEach(({ items }) =>
			items.forEach((item) => {
				const key = item.id.replace(' ', '_')
				const opt =
					item.validOptions?.filter((option) => option.default)[0] ||
					null
				const val = !!opt ? opt.id : item.default ? item.default : null
				baseData[key] = val
			})
		)
		setFormData(baseData)
	}, [data])

	const handleChange = ({ target }) => {
		// update inputs of type 'radio' on the DOM have another structure of ID / Value
		// which can mess up form vals on update, so they need personalized config
		let info = {}

		if (target.type === 'radio') {
			info[target.name] = target.id
		} else if (target.type === 'select-multiple') {
			// this 'multiple' stuff was hard, content is an array
			const options = target.options
			let value = []
			for (let i = 0, l = options.length; i < l; i++) {
				if (options[i].selected) {
					value.push(options[i].value)
				}
			}
			info[target.id] = value
		} else {
			info[target.id.replace(' ', '_')] = target.value
		}
		setFormData({
			...formData,
			...info
		})
	}

	return data.length ? (
		<>
			<form onChange={handleChange} onSubmit={(e) => e.preventDefault()}>
				{data.map((data, key) => (
					<div key={`cat-${key}`}>
						<Category data={data} />
					</div>
				))}
				<div>
					<Button onClick={saveFile}>Save JSON file</Button>
				</div>
			</form>
		</>
	) : (
		<center>Loading data...</center>
	)
}
export default Form
