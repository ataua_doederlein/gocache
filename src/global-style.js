import { createGlobalStyle } from 'styled-components'
import abz from './assets/fonts/ABeeZee-Regular.ttf'

const GlobalStyle = createGlobalStyle`
    @font-face {
        font-family: abz;
        src: url(${abz}) format('truetype');
        font-weight: normal;
        font-style: normal;
    }

    body {
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto',
            'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
            'Helvetica Neue', sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    body,
    body * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    code {
        font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
            monospace;
    }

    .App-logo {
        /* min-height: 2rem; */
        height: inherit;
        margin-left: 1rem;
        pointer-events: none;
    }

    header {
        min-height: 8vmin;
        padding: 2px;
        text-align: left;
        font-size: calc(10px + 2vmin);
        background: whitesmoke;
        position: sticky;
        top: 0;
        padding: 4px;
    }

    main { 
        padding: 20px 0;
    }

`

export default GlobalStyle
